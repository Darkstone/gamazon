﻿using Microsoft.AspNetCore.Mvc;
using Controller = Gamazon.Api.Http.Controller;

namespace Gamazon.Api.Areas.Products.Controllers
{
    [Area("Products")]
    public abstract class ProductsController : Controller
    {
    }
}