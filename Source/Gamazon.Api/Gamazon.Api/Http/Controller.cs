﻿using Microsoft.AspNetCore.Mvc;

namespace Gamazon.Api.Http
{
    [Route("[area]/[controller]")]
    public abstract class Controller : Microsoft.AspNetCore.Mvc.Controller
    {
    }
}
